import React from 'react';
import {Elevator} from "../model/Elevator";
import {TableCell, TableRow} from "@material-ui/core";
import {faArrowAltCircleDown, faArrowAltCircleUp, faMinus} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const ElevatorRow: React.FC<Elevator> = (props) => {
    const {id, currentFloor, direction} = props;

    const directionIcon = direction === "NONE" ? faMinus : direction === "UP" ? faArrowAltCircleUp : faArrowAltCircleDown;

    return (
        <TableRow>
            <TableCell>{id}</TableCell>
            <TableCell>{currentFloor}</TableCell>
            <TableCell><FontAwesomeIcon icon={directionIcon} /></TableCell>
        </TableRow>
    );
}

export default ElevatorRow;
