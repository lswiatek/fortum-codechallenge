import React from "react";
import {Elevator} from "../model/Elevator";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {createStyles, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@material-ui/core";
import ElevatorRow from "./ElevatorRow";
import {Skeleton} from "@material-ui/lab";

const useStyles = makeStyles(() =>
    createStyles({
        table: {
            minWidth: 650,
        }
    }));


const tableLoading = () => (
    <TableBody>
        {
            Array.from({length: 6}).map(() =>
                (
                    <TableRow key={Math.random()}>
                        <TableCell colSpan={3}><Skeleton/></TableCell>
                    </TableRow>
                ))
        }
    </TableBody>
);

const tableLoaded = (elevators: Array<Elevator>) =>
    (
        <TableBody>
            {
                elevators.map((elevator) => (
                    <ElevatorRow key={elevator.id} {...elevator} />
                ))
            }
        </TableBody>
    );


const ElevatorsTable: React.FC<{ elevators: Array<Elevator> }> = ({elevators}) => {
    const classes = useStyles();
    return (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>Elevator #</TableCell>
                        <TableCell>Current floor</TableCell>
                        <TableCell>Direction</TableCell>
                    </TableRow>
                </TableHead>
                {elevators.length ? tableLoaded(elevators) : tableLoading()}
            </Table>
        </TableContainer>
    );
}

export default ElevatorsTable;