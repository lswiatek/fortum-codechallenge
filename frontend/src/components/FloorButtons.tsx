import React from "react";
import {Button, ButtonGroup, createStyles, Theme, Typography} from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        buttons: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            '& > *': {
                margin: theme.spacing(1),
            },
        },
    }));

interface FloorButtonsProps {
    buttons: Array<number>;
    onButtonClick: (floor: number) => void
}

const FloorButtons: React.FC<FloorButtonsProps> = ({buttons, onButtonClick}) => {
    const classes = useStyles();

    return (
        <div className={classes.buttons}>
            <Typography variant="h5">Choose floor</Typography>
            <ButtonGroup color="primary" aria-label="outlined primary button group">
                {
                    buttons
                        .map(floor => <Button key={floor}
                                              onClick={() => onButtonClick(floor)}>{floor}</Button>)
                }
            </ButtonGroup>
        </div>
    );
}

export default FloorButtons;