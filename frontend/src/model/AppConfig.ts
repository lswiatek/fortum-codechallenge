export interface AppConfig {
    minFloor: number;
    maxFloor: number;
}