import {Observable} from "rxjs";
import {Elevator} from "../model/Elevator";
import {ajax} from "rxjs/ajax";
import {AppConfig} from "../model/AppConfig";
import {controllerEndpoint} from "../constatnts";

export function getElevators(): Observable<Array<Elevator>> {
    return ajax.getJSON(`http://${controllerEndpoint}/rest/v1/elevators`);
}

export function requestElevator(toFloor: number): Observable<Elevator> {
    return ajax.getJSON(`http://${controllerEndpoint}/rest/v1/request/${toFloor}`);
}

export function getConfig(): Observable<AppConfig> {
    return ajax.getJSON(`http://${controllerEndpoint}/rest/v1/config`);
}
