import React, {useEffect, useState} from 'react';
import {Container, Divider, IconButton, Snackbar} from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';
import {Elevator} from "./model/Elevator";
import {getConfig, getElevators, requestElevator} from "./services/ElevatorApi";
import {Client} from '@stomp/stompjs';
import {controllerEndpoint} from "./constatnts";
import FloorButtons from "./components/FloorButtons";
import ElevatorsTable from "./components/ElevatorsTable";
import {delay, mergeMap, retryWhen} from "rxjs/operators";

const App = () => {
    const [buttons, setButtons] = useState<Array<number>>([]);
    const [elevators, setElevators] = useState<Array<Elevator>>([]);
    const [snackbar, setSnackbar] = useState({open: false, message: ''});

    useEffect(() => {
        const client = new Client();

        client.configure({
            brokerURL: `ws://${controllerEndpoint}/ws`,
            onConnect: () => {
                client.subscribe('/elevators/status', message => {
                    const {id, direction, currentFloor}: Elevator = JSON.parse(message.body);
                    setElevators(state => state.map(e => e.id === id ? {...e, currentFloor, direction} : e));
                    if (direction === 'NONE') {
                        setSnackbar({
                            open: true,
                            message: `Elevator #${id} arrived.`
                        });
                    }
                })
            }
        });

        const controller$ = getConfig()
            .pipe(
                mergeMap(({minFloor, maxFloor}) => {
                        const buttons = Array.from({length: maxFloor - minFloor + 1}, (v, k) => minFloor + k);
                        setButtons(buttons);
                        return getElevators();
                    }
                ),
                retryWhen(errors => errors.pipe(delay(1000)))
            ).subscribe(data => {
                client.activate();
                setElevators(data);
            });

        return () => {
            controller$.unsubscribe();
            client.deactivate();
        };
    }, [])

    const handleRequestElevatorBtnClicked = (toFloor: number) => {
        requestElevator(toFloor)
            .subscribe((elevator: Elevator) => {
                if (elevator) {
                    const {id, direction, currentFloor} = elevator;
                    setElevators(state => state.map(e => e.id === id ? {...e, currentFloor, direction} : e));
                    setSnackbar({
                        open: true,
                        message: `Elevator #${id} is ${direction === 'NONE' ? 'ready' : 'coming ' + direction.toLocaleLowerCase()}.`
                    });
                }
            }, () => {
                setSnackbar({
                    open: true,
                    message: `Floor ${toFloor} does not exist.`
                });
            })
    }

    const closeSnackbar = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnackbar({open: false, message: ''});
    }

    return (
        <Container fixed>
            <ElevatorsTable elevators={elevators}/>

            <Divider/>

            <FloorButtons buttons={buttons} onButtonClick={handleRequestElevatorBtnClicked}/>

            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={snackbar.open}
                autoHideDuration={1000}
                onClose={closeSnackbar}
                message={snackbar.message}
                action={
                    <React.Fragment>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={closeSnackbar}>
                            <CloseIcon fontSize="small"/>
                        </IconButton>
                    </React.Fragment>
                }
            />

        </Container>
    );
}
export default App;
