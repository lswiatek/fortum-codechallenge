package com.fortum.codechallenge.elevators.backend.resources;

import com.fortum.codechallenge.elevators.backend.api.Elevator;
import com.fortum.codechallenge.elevators.backend.api.ElevatorController;
import com.fortum.codechallenge.elevators.backend.impl.ElevatorImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

/**
 * Boiler plate test class to get up and running with a test faster.
 */
@SpringBootTest
public class ElevatorControllerEndPointsTest {

    @Autowired
    private ElevatorControllerEndPoints endPoint;

    @MockBean
    private ElevatorController elevatorController;

    @Test
    public void ping() {
        String response = endPoint.ping();
        assertThat(response).isEqualTo("pong");
    }

    @Test
    public void shouldReturnListOfElevators() {
        when(elevatorController.getElevators()).thenReturn(List.of(new ElevatorImpl(1, 0, 11)));

        ResponseEntity<List<Elevator>> response = endPoint.getElevators();

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).hasSize(1);
    }

    @Test
    public void shouldReturnElevatorWhenRequestElevatorIfElevatorIsAvailable(){
        when(elevatorController.requestElevator(anyInt())).thenReturn(new ElevatorImpl(1, 0, 11));

        ResponseEntity<Elevator> response = endPoint.requestElevator(1);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).hasFieldOrPropertyWithValue("id", 1);
    }

    @Test
    public void shouldReturnNoContentWhenRequestElevatorIfNoElevatorIsAvailable(){
        when(elevatorController.requestElevator(anyInt())).thenReturn(null);

        ResponseEntity<Elevator> response = endPoint.requestElevator(1);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(response.getBody()).isNull();
    }

    @Test
    public void shouldReturnBadRequestWhenRequestElevatorIfNonExistingFloorIsRequestes(){
        doThrow(IllegalArgumentException.class).when(elevatorController).requestElevator(anyInt());

        ResponseEntity<Elevator> response = endPoint.requestElevator(-2);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody()).isNull();
    }
}
