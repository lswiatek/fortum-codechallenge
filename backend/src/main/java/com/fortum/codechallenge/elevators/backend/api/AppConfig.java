package com.fortum.codechallenge.elevators.backend.api;

public class AppConfig {
    private int minFloor;
    private int maxFloor;

    public AppConfig(int minFloor, int maxFloor) {
        this.minFloor = minFloor;
        this.maxFloor = maxFloor;
    }

    public int getMinFloor() {
        return minFloor;
    }

    public int getMaxFloor() {
        return maxFloor;
    }
}
