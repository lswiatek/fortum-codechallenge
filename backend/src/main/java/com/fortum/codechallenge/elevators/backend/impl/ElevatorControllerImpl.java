package com.fortum.codechallenge.elevators.backend.impl;

import com.fortum.codechallenge.elevators.backend.api.AppConfig;
import com.fortum.codechallenge.elevators.backend.api.Elevator;
import com.fortum.codechallenge.elevators.backend.api.ElevatorController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

@Service
public class ElevatorControllerImpl implements ElevatorController {
    private static final Logger logger = LoggerFactory.getLogger(ElevatorControllerImpl.class);

    private final SimpMessagingTemplate messagingTemplate;
    private final ExecutorService taskExecutor;
    private final int floorMoveTime;
    private final int minFloor;
    private final int maxFloor;
    private final List<Elevator> elevators;

    public ElevatorControllerImpl(@Autowired ExecutorService taskExecutor,
                                  @Autowired SimpMessagingTemplate messagingTemplate,
                                  @Value("${com.fortum.codechallenge.numberOfElevators}") int numOfElevators,
                                  @Value("${com.fortum.codechallenge.minFloor}") int minFloor,
                                  @Value("${com.fortum.codechallenge.maxFloor}") int maxFloor,
                                  @Value("${com.fortum.codechallenge.floorMoveTime}") int floorMooveTime) {
        this.taskExecutor = taskExecutor;
        this.messagingTemplate = messagingTemplate;
        this.floorMoveTime = floorMooveTime;
        this.minFloor = minFloor;
        this.maxFloor = maxFloor;
        this.elevators = new ArrayList<>();
        for (int i = 0; i < numOfElevators; i++) {
            elevators.add(new ElevatorImpl(i, minFloor, maxFloor));
        }
        logger.info("Created {} elevators which go from {} floor to {} floor", numOfElevators, minFloor, maxFloor);
    }

    @Override
    public Elevator requestElevator(int toFloor) {
        if (toFloor < this.minFloor || toFloor > this.maxFloor) {
            throw new IllegalArgumentException("Unable to move to non-existent floor " + toFloor);
        }

        List<Elevator> elevators = this.elevators.stream().filter(t -> !t.isBusy()).collect(Collectors.toList());
        if (elevators.isEmpty()) {
            logger.info("No elevators available at the moment. Request will be repeated.");
            taskExecutor.submit(() -> requestElevator(toFloor));
            return null;
        }

        Elevator elevator = elevators.stream()
                .min(Comparator.comparingInt(i -> Math.abs(i.getCurrentFloor() - toFloor))).get();

        logger.info("Elevator requested for floor {}. Sending elevator {}", toFloor, elevator.getId());
        elevator.moveElevator(toFloor);

        taskExecutor.submit(new ElevatorMoveTask(messagingTemplate, elevator, floorMoveTime));
        return elevator;
    }

    @Override
    public List<Elevator> getElevators() {
        return new ArrayList<>(this.elevators);
    }

    @Override
    public AppConfig getAppConfig() {
        return new AppConfig(minFloor, maxFloor);
    }
}
