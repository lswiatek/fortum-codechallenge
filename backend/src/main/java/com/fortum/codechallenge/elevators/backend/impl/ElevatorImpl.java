package com.fortum.codechallenge.elevators.backend.impl;

import com.fortum.codechallenge.elevators.backend.api.Elevator;
import java.util.concurrent.ThreadLocalRandom;

public class ElevatorImpl implements Elevator {
    private int addressedFloor;
    private final int id;
    private int currentFloor;

    public ElevatorImpl(int id, int minFloor, int maxFloor) {
        this.id = id;
        this.currentFloor = ThreadLocalRandom.current().nextInt(minFloor, maxFloor + 1);
        this.addressedFloor = this.currentFloor;
    }

    @Override
    public Direction getDirection() {
        if (getAddressedFloor() < getCurrentFloor()) {
            return Direction.DOWN;
        } else if (getAddressedFloor() > getCurrentFloor()) {
            return Direction.UP;
        } else {
            return Direction.NONE;
        }
    }

    @Override
    public int getAddressedFloor() {
        return this.addressedFloor;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void moveElevator(int toFloor) {
        addressedFloor = toFloor;
    }

    @Override
    public boolean isBusy() {
        return getCurrentFloor() != getAddressedFloor();
    }

    @Override
    public int getCurrentFloor() {
        return this.currentFloor;
    }

    @Override
    public void moveFloor() {
        if (Direction.UP.equals(getDirection())) {
            setCurrentFloor(getCurrentFloor() + 1);
        }
        if (Direction.DOWN.equals(getDirection())) {
            setCurrentFloor(getCurrentFloor() - 1);
        }
    }

    public void setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
    }
}
