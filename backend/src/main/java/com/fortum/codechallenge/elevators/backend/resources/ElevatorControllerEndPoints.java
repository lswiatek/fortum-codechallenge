package com.fortum.codechallenge.elevators.backend.resources;

import com.fortum.codechallenge.elevators.backend.api.AppConfig;
import com.fortum.codechallenge.elevators.backend.api.Elevator;
import com.fortum.codechallenge.elevators.backend.api.ElevatorController;
import com.fortum.codechallenge.elevators.backend.impl.ElevatorImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Rest Resource.
 */
@RestController
@RequestMapping("/rest/v1")
@CrossOrigin
public final class ElevatorControllerEndPoints {

    @Autowired
    private ElevatorController elevatorController;

    /**
     * Ping service to test if we are alive.
     *
     * @return String pong
     */
    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    public String ping() {
        return "pong";
    }

    /**
     * Get all elevators
     *
     * @return list of elevators
     */
    @GetMapping(value = "/elevators")
    public ResponseEntity<List<Elevator>> getElevators(){
        return ResponseEntity.ok(elevatorController.getElevators());
    }

    /**
     * Request elevator to given floor
     *
     * @param toFloor destination floor
     *
     * @return
     *  HTTP status 200 if elevator is found
     *  HTTP status 204 if no elevator is available at the moment
     *  HTTP status 400 if non-existent floor requested
     */
    @GetMapping(value = "/request/{toFloor}")
    public ResponseEntity<Elevator> requestElevator(@PathVariable int toFloor) {
        try {
            Elevator elevator = elevatorController.requestElevator(toFloor);
            if (elevator != null) {
                return ResponseEntity.ok(elevator);
            }
        } catch (IllegalArgumentException iae){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/config")
    public ResponseEntity<AppConfig> getConfig() {
        return ResponseEntity.ok(elevatorController.getAppConfig());
    }

}
