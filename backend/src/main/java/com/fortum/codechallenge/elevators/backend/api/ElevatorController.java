package com.fortum.codechallenge.elevators.backend.api;

import java.util.List;


/**
 * Interface for the Elevator Controller.
 */
public interface ElevatorController {

    /**
     * Request an elevator to the specified floor.
     *
     * @param toFloor addressed floor as integer.
     * @return The Elevator that is going to the floor, if there is one to move.
     */
    Elevator requestElevator(int toFloor);

    /**
     * A snapshot list of all elevators in the system.
     *
     * @return A List with all {@link Elevator} objects.
     */
    List<Elevator> getElevators();

    /**
     * Object with min and max floor
     *
     * @return AppConfig
     */
    AppConfig getAppConfig();

}
