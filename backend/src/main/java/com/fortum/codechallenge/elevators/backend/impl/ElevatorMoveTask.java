package com.fortum.codechallenge.elevators.backend.impl;

import com.fortum.codechallenge.elevators.backend.api.Elevator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.concurrent.Callable;

public class ElevatorMoveTask implements Callable<Elevator> {
    private static final Logger logger = LoggerFactory.getLogger(ElevatorMoveTask.class);

    private final SimpMessagingTemplate messagingTemplate;
    private final Elevator elevator;
    private final int floorMoveTime;

    public ElevatorMoveTask(SimpMessagingTemplate messagingTemplate, Elevator elevator, int floorMoveTime) {
        this.messagingTemplate = messagingTemplate;
        this.elevator = elevator;
        this.floorMoveTime = floorMoveTime;
    }

    @Override
    public Elevator call() throws Exception {
        logger.info("Elevator is going {} from {} floor.", elevator.getDirection(), elevator.getCurrentFloor());

        while (this.elevator.isBusy()) {
            Thread.sleep(floorMoveTime);
            elevator.moveFloor();
            logger.info("Elevator is at {} floor.", elevator.getCurrentFloor());
            messagingTemplate.convertAndSend("/elevators/status", elevator);
        }

        logger.info("Elevator arrived.");
        return elevator;
    }
}
